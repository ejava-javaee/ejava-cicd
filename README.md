# ejava-cicd

Contains utilities for building ejava projects in gitlab.

## Getting started

Create a .gitlab-ci.yml file in your repository and include the CI/CD scripts.

```
include:
  - project: 'ejava-javaee/ejava-cicd'
    ref: main
    file: 'gitlab-ci-maven.yml'
```
